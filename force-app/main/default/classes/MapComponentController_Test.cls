@isTest
public with sharing class MapComponentController_Test {

    private static Integer NUM_OF_ACCOUNTS_WITH_LOCATION = 4;
    private static Integer NUM_OF_CONTACTS_WITH_LOCATION = 4;
    private static Integer NUM_OF_USERS_WITH_LOCATION = [SELECT COUNT() FROM User WHERE Latitude != null AND Longitude != null];
    private static Decimal SEARCH_LATITUDE = 45.0;
    private static Decimal SEARCH_LONGITUDE = -95.0;

    @TestSetup
    static void makeData(){
        // Accounts
        List<Account> accs = new List<Account>();
        accs.add(new Account(Name='Adam\'s Apples', ShippingLatitude=45.0, ShippingLongitude=-95.0));
        accs.add(new Account(Name='Bud\'s Bar', ShippingLatitude=45.1, ShippingLongitude=-95.1));
        accs.add(new Account(Name='Chuck\'s Candles', ShippingLatitude=45.2, ShippingLongitude=-95.2));
        accs.add(new Account(Name='Diana\'s Diamonds', ShippingLatitude=45.3, ShippingLongitude=-95.3));
        insert accs;
        // Contacts
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName='Bass', MailingLatitude=46.0, MailingLongitude=-96.0));
        cons.add(new Contact(LastName='Finley', MailingLatitude=46.1, MailingLongitude=-96.1));
        cons.add(new Contact(LastName='Adams', MailingLatitude=46.2, MailingLongitude=-96.2));
        cons.add(new Contact(LastName='Fry', MailingLatitude=46.3, MailingLongitude=-96.3));
        insert cons;

    }
    
    @isTest
    static void doesGetNearbyLocationsGetAllObjects()
    {
        MapComponentController.CoordinatePair coords = new MapComponentController.CoordinatePair();
        coords.latitude = SEARCH_LATITUDE;
        coords.longitude = SEARCH_LONGITUDE;
        Test.startTest();
        List<MapComponentController.LocationResponse> res = MapComponentController.getNearbyLocations(coords);
        Test.stopTest();

        for(MapComponentController.LocationResponse x : res) {
            System.debug(x);
        }
        System.assertEquals(NUM_OF_ACCOUNTS_WITH_LOCATION + NUM_OF_CONTACTS_WITH_LOCATION + NUM_OF_USERS_WITH_LOCATION, res.size(),
                            'Incorrect number of locations found.');
    }
}
