public with sharing class MapComponentController {

    private static Integer MAX_DISTANCE = 50;
    
    @AuraEnabled(cacheable=true)
    public static List<LocationResponse> getNearbyLocations(CoordinatePair coords)
    {
        System.debug(coords);
        List<LocationResponse> res = new List<LocationResponse>();
        // Get nearby Accounts
        List<Account> nearbyAccs = [SELECT Id, Name, Description, ShippingLatitude, ShippingLongitude,
                                       ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,
                                       DISTANCE(ShippingAddress,GEOLOCATION(:coords.latitude, :coords.longitude), 'mi') distance
                                        FROM Account
                                        WHERE ShippingLatitude != null AND ShippingLongitude != null
                                        AND DISTANCE(ShippingAddress,
                                            GEOLOCATION(:coords.latitude, :coords.longitude), 'mi') < :MAX_DISTANCE
                                        ORDER BY DISTANCE(ShippingAddress,
                                            GEOLOCATION(:coords.latitude, :coords.longitude), 'mi')
                                        LIMIT 100];

        for(Account eachAcc : nearbyAccs) {
            res.add(new LocationResponse(eachAcc));
        }
        // Get nearby Contacts
        List<Contact> nearbyCons = [SELECT Id, Name, Description, MailingLatitude, MailingLongitude,
                                       MailingStreet, MailingCity, MailingState, MailingPostalCode,
                                       DISTANCE(MailingAddress, GEOLOCATION(:coords.latitude, :coords.longitude), 'mi') distance
                                        FROM Contact
                                        WHERE MailingLatitude != null AND MailingLongitude != null
                                        AND DISTANCE(MailingAddress,
                                            GEOLOCATION(:coords.latitude, :coords.longitude), 'mi') < :MAX_DISTANCE
                                        ORDER BY DISTANCE(MailingAddress,
                                            GEOLOCATION(:coords.latitude, :coords.longitude), 'mi')
                                        LIMIT 100];

        for(Contact eachCon : nearbyCons) {
            res.add(new LocationResponse(eachCon));
        }

        //  Get nearby Users
        List<User> nearbyUsers = [SELECT Id, Name, AboutMe, Latitude, Longitude,
                                       Street, City, State, PostalCode,
                                       DISTANCE(Address, GEOLOCATION(:coords.latitude, :coords.longitude), 'mi') distance
                                        FROM User
                                        WHERE Latitude != null AND Longitude != null
                                        AND DISTANCE(Address,
                                            GEOLOCATION(:coords.latitude, :coords.longitude), 'mi') < :MAX_DISTANCE
                                        ORDER BY DISTANCE(Address,
                                            GEOLOCATION(:coords.latitude, :coords.longitude), 'mi')
                                        LIMIT 100];

        for(User eachUser : nearbyUsers) {
            res.add(new LocationResponse(eachUser));
        }

        res.sort();

        // See result
        for(LocationResponse eachRes : res) {
            System.debug(eachRes);
        }
        return res;
    }

    public class CoordinatePair {
        @AuraEnabled
        public Decimal latitude {get; set;}
        @AuraEnabled
        public Decimal longitude {get; set;}
    }

    public class LocationResponse implements Comparable{
        @AuraEnabled
        public String type {get; set;}
        @AuraEnabled 
        public String icon {get;set;} 
        @AuraEnabled 
        public String title {get;set;} 
        @AuraEnabled
        public String description {get;set;} 
        @AuraEnabled
        public Decimal distance {get; set;}
        @AuraEnabled 
        public GeoLocation location {get;set;} 

        public LocationResponse(Account acc) {
            icon = 'standard:account';
            type = 'account';
            title = acc.Name;
            description = acc.Description;
            distance = (Decimal)acc.get('distance');
            location = new GeoLocation(acc);
        }

        public LocationResponse(Contact con) {
            icon = 'standard:contact';
            type = 'contact';
            title = con.Name;
            description = con.Description;
            distance = (Decimal)con.get('distance');
            location = new GeoLocation(con);
        }

        public LocationResponse(User usr) {
            icon = 'standard:user';
            type = 'user';
            title = usr.Name;
            description = usr.AboutMe;
            distance = (Decimal)usr.get('distance');
            location = new GeoLocation(usr);
        }

        public Integer compareTo(Object objectToCompareTo)
        {
            LocationResponse that = (LocationResponse)objectToCompareTo;
            if(this.distance != null && that.distance != null) {
                return Integer.valueOf(this.distance - that.distance);
            } else {
                return -1;
            }
        }
    }

    public class GeoLocation{
        @AuraEnabled 
        public String Street {get;set;}
        @AuraEnabled 
        public String PostalCode {get;set;}
        @AuraEnabled 
        public String City {get;set;}
        @AuraEnabled 
        public String State {get;set;}
        @AuraEnabled 
        public String Country {get;set;}
        @AuraEnabled 
        public decimal Latitude {get;set;}
        @AuraEnabled 
        public decimal Longitude {get;set;}

        public GeoLocation(Account acc) {
            Street = acc.ShippingStreet;
            PostalCode = acc.ShippingPostalCode;
            City = acc.ShippingCity;
            State = acc.ShippingState;
            Latitude = acc.ShippingLatitude;
            Longitude = acc.ShippingLongitude;
        }

        public GeoLocation(Contact con) {
            Street = con.MailingStreet;
            PostalCode = con.MailingPostalCode;
            City = con.MailingCity;
            State = con.MailingState;
            Latitude = con.MailingLatitude;
            Longitude = con.MailingLongitude;
        }

        public GeoLocation(User usr) {
            Street = usr.Street;
            PostalCode = usr.PostalCode;
            City = usr.City;
            State = usr.State;
            Latitude = usr.Latitude;
            Longitude = usr.Longitude;
        }
    }

    
}
