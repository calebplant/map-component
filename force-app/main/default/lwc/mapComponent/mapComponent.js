import { LightningElement, wire } from 'lwc';
import getNearbyLocations from '@salesforce/apex/MapComponentController.getNearbyLocations'

export default class MapComponent extends LightningElement {
    searchLocation;
    mapMarkers;
    filteredMarkers;
    zoomLevel;
    address;

    showAccount = true;
    showContact = true;
    showUser = true;

    resultPerPage=5;
    totalPages;
    currentPage;

    @wire(getNearbyLocations, {coords: '$searchLocation'})
    runSearch(response) {
        if(response.data) {
            console.log('Response from server:');
            console.log(response.data);
            if(response.data.length > 0) {
                this.mapMarkers = response.data;
                this.updateFilteredMarkers();
                this.updatePagination();
            } else {
                this.filteredMarkers = [];
                this.mapMarkers = [];
            }
        }
        if(response.error) {
            console.log('Error searching for locations!');
            console.log(response.error);
        }
    }

    get showMap() {
        return (this.totalPages != null && this.resultPerPage != null && this.currentPage != null && this.filteredMarkers != null);
    }

    get displayedMarkers() {
        if(this.filteredMarkers) {
            let start = Number(this.currentPage) * Number(this.resultPerPage);
            let end = Number(start) + Number(this.resultPerPage);
            return this.filteredMarkers.slice(start, end);
        }
    }

    get nextDisabled() {
        return (this.currentPage == (this.totalPages - 1) || this.currentPage == undefined);
    }

    get previousDisabled() {
        return (this.currentPage === 0 || this.currentPage == undefined);
    }

    get pageLabel() {
        if(this.filteredMarkers && this.resultPerPage) {
            let start = Number(this.currentPage) * Number(this.resultPerPage) + 1;
            let end = Number(start) + Number(this.resultPerPage) - 1;
            end = end > this.filteredMarkers.length ? this.filteredMarkers.length : end;

            return 'Displaying ' + start + ' - ' + end + ' of ' + this.filteredMarkers.length;
        }
        return '';
    }

    get showNoResultMessage() {
        if(this.filteredMarkers) {
            return this.filteredMarkers.length > 0 ? false : true;
        }
        return false;
    }

    connectedCallback() {
        console.log('mapComponent :: connectedCallback');
        // Default to user's location as center
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                console.log('Setting coordinates');
                this.zoomLevel = 10;
                this.searchLocation = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
            });
        }
    }

    // Handlers
    handleSearchChange(event) {
        console.log('mapComponent :: handleSearchChange');
        this.address = {
            street: event.detail.street,
            city: event.detail.city,
            province: event.detail.province,
            postalCode: event.detail.postalCode,
        }
    }

    handleSearch() {
        console.log('mapComponent :: handleSearch');
           const key = 'AIzaSyCBEujaPUVCsCRAOsvouVuOY9nzM2M4WNk'; //placeholder
           let address = this.buildAddress();
           const url =  `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${key}`;

           return fetch(url,
                {
                    method: "GET" 
                })
               .then((data) => { 
                   return data.json(); 
                })
               .then((response) => {
                   console.log('response');
                    console.log(response);
                    if(response.status === 'OK') {
                        let geoData = response.results[0].geometry.location;
                        this.searchLocation = {
                            latitude: geoData.lat,
                            longitude: geoData.lng
                        }
                    }
               }).catch((error) => { 
                    console.log(error);
                    this.mapMarkers = [];
                    this.filteredMarkers = [];
               });   
    }

    handleToggleFilter(event) {
        console.log('mapComponent :: handleToggleAccount');

        switch(event.detail) {
            case 'account':
                this.showAccount = !this.showAccount;
                break;
            case 'contact':
                this.showContact = !this.showContact;
                break;
            case 'user':
                this.showUser = !this.showUser;
                break;
        }
        this.updateFilteredMarkers();
        this.updatePagination();
    }

    handlePageSizeChange(event) {
        console.log('mapComponent :: handlePageSizeChange');
        this.resultPerPage = event.detail;
        this.updatePagination();
    }

    handlePageChange(event) {
        console.log('mapComponent :: handlePageChange');
        switch(event.currentTarget.name){
            case 'previous':
                this.currentPage--;
                break;
            case 'next':
                this.currentPage++;
                break;
        }
    }

    // Utils
    updateFilteredMarkers() {
        if(this.mapMarkers) {
            this.filteredMarkers = this.mapMarkers.filter(eachMarker => {
                if(
                    (this.showAccount && eachMarker.type === 'account') ||
                    (this.showContact && eachMarker.type === 'contact') ||
                    (this.showUser && eachMarker.type === 'user')) {
                    return true;
                }
            });
        }
    }

    updatePagination() {
        this.currentPage = 0;
        this.totalPages = Math.ceil(this.filteredMarkers.length / this.resultPerPage);
    }

    buildAddress() {
        let result = '';
        if(this.address.street) result += this.address.street.replace(/\s/g, '+') + ',';
        if(this.address.city) result += this.address.city.replace(/\s/g, '+') + ',';
        if(this.address.province) result += this.address.province + '+';
        if(this.address.postalCode) result += this.address.postalCode;
        return result;
    }
}