import { api, LightningElement } from 'lwc';

export default class MapHeader extends LightningElement {

    @api accountActive;
    @api contactActive;
    @api userActive;

    selectedValue=5;

    get pageSizeOptions() {
        return [
            {label: 5, value: 5},
            {label: 10, value: 10},
            {label: 25, value: 25},
            {label: 50, value: 50},
            {label: 75, value: 75},
        ]
    }

    // Handlers
    handleFilterClick(event) {
        console.log('MapHeader :: handleAccountClick');
        this.dispatchEvent(new CustomEvent('togglefilter', {detail: event.currentTarget.name}));
    }

    handlePageSizeChange(event) {
        console.log('MapHeader :: handlePageSizeChange');
        this.selectedValue = event.detail.value;
        this.dispatchEvent(new CustomEvent('pagesizechange', {detail: event.detail.value}));
    }
}